# ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

resource "aws_iam_user" "default" {

  count = local.create ? 1 : 0

  name                 = var.user_name
  path                 = var.path

  force_destroy        = var.force_destroy
  permissions_boundary = var.permissions_boundary

  tags = var.tags
}

resource "aws_iam_user_login_profile" "default" {

  count = local.create && var.create_user_login_profile ? 1 : 0

  user                    = aws_iam_user.default[0].name
  pgp_key                 = var.pgp_key
  password_length         = var.password_length
  password_reset_required = var.password_reset_required

  # TODO: Remove once https://github.com/hashicorp/terraform-provider-aws/issues/23567 is resolved
  lifecycle {
    ignore_changes = [password_reset_required]
  }
}

resource "aws_iam_access_key" "default" {

  count = local.create && var.create_user_access_key && var.pgp_key != "" ? 1 : 0

  user    = aws_iam_user.default[0].name
  pgp_key = var.pgp_key
  status  = var.iam_access_key_status
}

resource "aws_iam_access_key" "no_pgp" {

  count = local.create && var.create_user_access_key && var.pgp_key == "" ? 1 : 0

  user   = aws_iam_user.default[0].name
  status = var.iam_access_key_status
}

resource "aws_iam_user_ssh_key" "default" {

  count = local.create && var.upload_iam_user_ssh_key ? 1 : 0

  username   = aws_iam_user.default[0].name
  encoding   = var.ssh_key_encoding
  public_key = var.ssh_public_key
}

