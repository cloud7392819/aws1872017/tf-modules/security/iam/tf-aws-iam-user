# ws@2023 ssm.tf

##################################################################
# SSM
##################################################################

##################################################################
## create ssm parameter iam login profile
##################################################################

resource "aws_ssm_parameter" "login_profile" {

  count = local.create && var.create_user_login_profile && var.create_ssm_parameter_login_profile ? 1 : 0

  name  = "/iam/users/${var.user_name}/login"

  description = "Creds for ${var.user_name}"

  type = "SecureString"

  value = jsonencode({
	  USER_PASSWD = aws_iam_user_login_profile.default[0].password
    USER_NAME   = aws_iam_user_login_profile.default[0].user
  })
}

##################################################################
## create ssm parameter iam access key
##################################################################

resource "aws_ssm_parameter" "access_key" {

  count = local.create && var.create_user_access_key && var.create_ssm_parameter_login_profile ? 1 : 0

  name  = "/iam/users/${var.user_name}/key"

  description = "Creds for ${var.user_name}"

  type = "SecureString"

  value = jsonencode({
	  USER_NAME              = aws_iam_access_key.no_pgp[0].user    
    USER_ACCESS_KEY_ID     = aws_iam_access_key.no_pgp[0].id
    USER_SECRET_ACCESS_KEY = aws_iam_access_key.no_pgp[0].secret
  })
}
