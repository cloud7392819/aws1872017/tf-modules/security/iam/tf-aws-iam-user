# ws@2023 locals.tf

##################################################################
# LOCALS
##################################################################

locals {

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false
    
    tags = merge(
        var.module_tags,
        {
        Terraform = true
        }
    )
}

locals {
  has_encrypted_password             = length(compact(aws_iam_user_login_profile.default[*].encrypted_password)) > 0
  has_encrypted_secret               = length(compact(aws_iam_access_key.default[*].encrypted_secret)) > 0
  has_encrypted_ses_smtp_password_v4 = length(compact(aws_iam_access_key.default[*].encrypted_ses_smtp_password_v4)) > 0
}