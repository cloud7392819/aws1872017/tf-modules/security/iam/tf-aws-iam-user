# Define Local Values in Terraform

locals {

  # Region
  aws_region = "<region>"

  # Application
  name     = "<name>"
  cut_name = "<name>"

  # Environment [info]
  environment = "ecs"
  ownerEmail  = "user+xxx@doamin.com"
  team        = "DevOps"
  deployedby  = "Terraform"

}
