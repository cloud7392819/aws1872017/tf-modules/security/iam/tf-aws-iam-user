# ws@2023 providers.tf

##################################################################
# Terraform Block
##################################################################

terraform {
  required_version = ">= 1.9"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0"
    }
    #random = ">= 2.1"
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket                      = "<name>-tf-remote-state-all-<region>"
    key                         = "tfstate/<region>/iam/users/terraform.tfstate"
    region                      = "<region>"
    dynamodb_table              = "<name>-tf-remote-state-locks-<region>"
    encrypt                     = true
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

##################################################################
# Terraform Providers
##################################################################

provider "aws" {

  region = local.aws_region

  default_tags {

    tags = {
      "Environment" = local.environment
      "Team"        = local.team
      "DeployedBy"  = local.deployedby
      "Application" = local.name
      "OwnerEmail"  = local.ownerEmail
      "Region"      = local.aws_region
    }
  }
}

provider "aws" {

  alias  = "virginia"
  region = "us-east-1"

  default_tags {

    tags = {
      "Environment" = local.environment
      "Team"        = local.team
      "DeployedBy"  = local.deployedby
      "Application" = local.name
      "OwnerEmail"  = local.ownerEmail
      "Region"      = "us-east-1"
    }
  }
}