# ###############################################################
# Create IAM User: vasya_pupkin
# ##############################################################

module "iam_user_vasya_pupkin" {

  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-user.git?ref=tags/0.0.2"
  
  # #################################################################
  # Common/General settings
  # #################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = true

  user_name     = "vasya_pupkin"
  force_destroy = true

  create_user_login_profile = false
  create_user_access_key    = true

  create_ssm_parameter_login_profile = true

  #create_user_ssh_key
  #pgp_key = "keybase:vasya_pupkin"
  
  password_reset_required = false
}